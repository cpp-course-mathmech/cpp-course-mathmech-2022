#include <catch2/catch_all.hpp>


#include "mergeSort.h"
#include <algorithm>
#include <vector>
#include <numeric>
#include <random>



TEST_CASE("MergeSort::Public::SortsEmptyVector", "") {
    {
        int n = 9;
        int start = 0;
        std::vector<int> expected(n);
        std::iota(begin(expected), end(expected), start);

        std::vector<int> numbers = expected;

        auto rng = std::default_random_engine {};
        std::shuffle(std::begin(numbers), std::end(numbers), rng);

        MergeSort2(std::begin(numbers), std::end(numbers));

        REQUIRE(numbers == expected);
    }
}

TEST_CASE("MergeSort::Public::SortsVectorsWithLengthMultipleOfTwo", "") {
    {
        int n = 16;
        for (int i = 0; i < n; i++) {
            std::vector<int> expected(n);
            std::iota(begin(expected), end(expected), i);

            std::vector<int> numbers = expected;

            auto rng = std::default_random_engine {};
            std::shuffle(std::begin(numbers), std::end(numbers), rng);

            MergeSort2(std::begin(numbers), std::end(numbers));

            REQUIRE(numbers == expected);
        }
    }

    {
        int n = 32;
        for (int i = 0; i < n; i++) {
            std::vector<int> expected(n);
            std::iota(begin(expected), end(expected), i);

            std::vector<int> numbers = expected;

            auto rng = std::default_random_engine {};
            std::shuffle(std::begin(numbers), std::end(numbers), rng);

            MergeSort2(std::begin(numbers), std::end(numbers));

            REQUIRE(numbers == expected);
        }
    }
}

TEST_CASE("MergeSort::Public::SortsVectorsWithLengthMultipleOfThree", "") {
    {
        int n = 9;
        for (int i = 0; i < n; i++) {
            std::vector<int> expected(n);
            std::iota(begin(expected), end(expected), i);

            std::vector<int> numbers = expected;

            auto rng = std::default_random_engine {};
            std::shuffle(std::begin(numbers), std::end(numbers), rng);

            MergeSort2(std::begin(numbers), std::end(numbers));

            REQUIRE(numbers == expected);
        }
    }

    {
        int n = 27;
        for (int i = 0; i < n; i++) {
            std::vector<int> expected(n);
            std::iota(begin(expected), end(expected), i);

            std::vector<int> numbers = expected;

            auto rng = std::default_random_engine {};
            std::shuffle(std::begin(numbers), std::end(numbers), rng);

            MergeSort2(std::begin(numbers), std::end(numbers));

            REQUIRE(numbers == expected);
        }
    }
}

TEST_CASE("MergeSort::Public::SortsFastEnough", "") {
    std::vector<int> numbers(177147);
    std::iota(rbegin(numbers), rend(numbers), 0);

    {
        std::vector<int> numbers1 = numbers;
        auto t0 = std::chrono::high_resolution_clock::now();
        MergeSort2(begin(numbers1), end(numbers1));
        auto t1 = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0);
        long long msCount = duration.count();
        REQUIRE(msCount < 1000);
    }
    {
        std::vector<int> numbers1 = numbers;
        auto t0 = std::chrono::high_resolution_clock::now();
        MergeSort3(begin(numbers1), end(numbers1));
        auto t1 = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0);
        long long msCount = duration.count();
        REQUIRE(msCount < 1000);
    }
}
