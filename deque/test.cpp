#include <catch2/catch_all.hpp>


#include "deque.h"


TEST_CASE("DEQUE::Public::DefaultConstructorExists", "") {
    Deque<int> d;
    REQUIRE(true);
}

TEST_CASE("DEQUE::Public::ConstDequeEmptyReturnsTrueIfNoElements", "") {
    Deque<int> deque;
    auto &constDeque = const_cast<const Deque<int> &>(deque);
    REQUIRE(constDeque.Empty());
}

TEST_CASE("DEQUE::Public::ConstDequeSizeReturnsCountOfElements", "") {
    Deque<int> deque;
    auto &constDeque = const_cast<const Deque<int> &>(deque);
    REQUIRE(constDeque.Size() == 0);
    deque.PushBack(1);
    REQUIRE(constDeque.Size() == 1);
    deque.PushBack(0);
    REQUIRE(constDeque.Size() == 2);
}

TEST_CASE("DEQUE::Public::ConstDequeIndicesWorks", "") {
    Deque<int> deque;
    deque.PushBack(0);
    deque.PushBack(1);
    deque.PushBack(2);
    auto &constDeque = const_cast<const Deque<int> &>(deque);
    REQUIRE(deque[0] == 0);
    REQUIRE(constDeque[0] == 0);
    REQUIRE(deque[2] == 2);
    REQUIRE(constDeque[2] == 2);
}

TEST_CASE("DEQUE::Public::ConstDequeAtWorksAndThrowsOutOfRangeIfNeeded", "") {
    Deque<int> deque;
    deque.PushBack(0);
    deque.PushBack(1);
    auto &constDeque = const_cast<const Deque<int> &>(deque);
    REQUIRE(deque.At(0) == 0);
    REQUIRE(constDeque.At(0) == 0);
    REQUIRE_THROWS_AS(deque.At(2), std::out_of_range);
    REQUIRE_THROWS_AS(constDeque.At(2), std::out_of_range);
}

TEST_CASE("DEQUE::Public::ConstDequeFronBackWorks", "") {
    Deque<int> deque;
    deque.PushBack(0);
    deque.PushBack(1);
    deque.PushBack(2);
    auto &constDeque = const_cast<const Deque<int> &>(deque);
    REQUIRE(deque.Front() == 0);
    REQUIRE(constDeque.Front() == 0);

    REQUIRE(deque.Back() == 2);
    REQUIRE(constDeque.Back() == 2);
}

TEST_CASE("DEQUE::Public::ConstDequePushBackPushFrontWorks", "") {
    Deque<int> deque;
    deque.PushBack(2);
    deque.PushBack(3);
    deque.PushFront(1);
    deque.PushFront(0);

    REQUIRE(deque.At(0) == 0);
    REQUIRE(deque.At(1) == 1);
    REQUIRE(deque.At(2) == 2);
    REQUIRE(deque.At(3) == 3);
}

