#include <catch2/catch_all.hpp>


#include "josephusPermutation.h"
#include <vector>
#include <numeric>


struct NoncopyableInt {
    int value;

    NoncopyableInt(int value) : value(value) {}

    NoncopyableInt(const NoncopyableInt&) = delete;
    NoncopyableInt& operator=(const NoncopyableInt&) = delete;

    NoncopyableInt(NoncopyableInt&&) = default;
    NoncopyableInt& operator=(NoncopyableInt&&) = default;
};

bool operator == (const NoncopyableInt& lhs, const NoncopyableInt& rhs);


TEST_CASE("JosephusPermutation::Public::PermutesNoncopyableCorrectly", "") { // 
    std::vector<NoncopyableInt> numbers;
    numbers.push_back({1});
    numbers.push_back({2});
    numbers.push_back({3});
    numbers.push_back({4});
    numbers.push_back({5});

    MakeJosephusPermutation(begin(numbers), end(numbers), 2);

    std::vector<NoncopyableInt> expected;
    expected.push_back({1});
    expected.push_back({3});
    expected.push_back({5});
    expected.push_back({4});
    expected.push_back({2});

    REQUIRE(numbers == expected);
}

TEST_CASE("JosephusPermutation::Public::PermutesIntCorrectly", "") {
    {
        std::vector<int> numbers(10);
        std::iota(begin(numbers), end(numbers), 0);
        std::vector<int> expected = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        MakeJosephusPermutation(begin(numbers), end(numbers), 1);
        REQUIRE(numbers == expected);
    }
    {
        std::vector<int> numbers(10);
        std::iota(begin(numbers), end(numbers), 0);
        std::vector<int> expected = {0, 2, 4, 6, 8, 1, 5, 9, 7, 3};
        MakeJosephusPermutation(begin(numbers), end(numbers), 2);
        REQUIRE(numbers == expected);
    }
    {
        std::vector<int> numbers(10);
        std::iota(begin(numbers), end(numbers), 0);
        std::vector<int> expected = {0, 3, 6, 9, 4, 8, 5, 2, 7, 1};
        MakeJosephusPermutation(begin(numbers), end(numbers), 3);
        REQUIRE(numbers == expected);
    }
    {
        std::vector<int> numbers(10);
        std::iota(begin(numbers), end(numbers), 0);
        std::vector<int> expected = {0, 4, 8, 3, 9, 6, 5, 7, 2, 1};
        MakeJosephusPermutation(begin(numbers), end(numbers), 4);
        REQUIRE(numbers == expected);
    }
    {
        std::vector<int> numbers(10);
        std::iota(begin(numbers), end(numbers), 0);
        std::vector<int> expected = {0, 5, 1, 7, 4, 3, 6, 9, 2, 8};
        MakeJosephusPermutation(begin(numbers), end(numbers), 5);
        REQUIRE(numbers == expected);
    }
    {
        std::vector<int> numbers(10);
        std::iota(begin(numbers), end(numbers), 0);
        std::vector<int> expected = {0, 1, 3, 6, 2, 9, 5, 7, 4, 8};
        MakeJosephusPermutation(begin(numbers), end(numbers), 10);
        REQUIRE(numbers == expected);
    }
}
