#include <catch2/catch_all.hpp>


#include "macrosLog.h"
#include <sstream>
#include <string>
#include <iostream>


TEST_CASE("MacrosLog::Public::LogsMessagesWithLineSpecified", "") {
#line 1 "logger.cpp"
    std::ostringstream logs;
    Logger l(logs);
    l.SetLogLine(true);
    LOG(l, "a");
    LOG(l, "b");
    LOG(l, "c");
    LOG(l, "d");
    LOG(l, "e");
    std::string expected = "Line 4 a\n";
    expected += "Line 5 b\n";
    expected += "Line 6 c\n";
    expected += "Line 7 d\n";
    expected += "Line 8 e\n";

    REQUIRE(logs.str() == expected);
}

TEST_CASE("MacrosLog::Public::LogsMessagesWithFileSpecified", "") {
#line 1 "logger.cpp"
    std::ostringstream logs;
    Logger l(logs);
    l.SetLogFile(true);
    LOG(l, "message");
    
    std::string expected = "logger.cpp message\n";

    REQUIRE(logs.str() == expected);
}

TEST_CASE("MacrosLog::Public::LogsMessagesWithFileAndFileSpecified", "") { // 
#line 1 "logger.cpp"
    std::ostringstream logs;
    Logger l(logs);
    l.SetLogFile(true);
    l.SetLogLine(true);
    LOG(l, "message");
    std::string expected = "logger.cpp:5 message\n";

    REQUIRE(logs.str() == expected);
}

TEST_CASE("MacrosLog::Public::LogsMessagesAccordingToConfiguration", "") {
#line 1 "logger.cpp"
    std::ostringstream logs;
    Logger l(logs);
    LOG(l, "message1");

    l.SetLogFile(true);
    LOG(l, "message2");

    l.SetLogLine(true);
    LOG(l, "message3");

    l.SetLogFile(false);
    LOG(l, "message4");

    std::string expected = "message1\n";
    expected += "logger.cpp message2\n";
    expected += "logger.cpp:9 message3\n";
    expected += "Line 12 message4\n";

    REQUIRE(logs.str() == expected);
}

TEST_CASE("MacrosLog::Public::LogsMessagesInIfElse", "") {
    std::ostringstream logs;
    Logger l(logs);
    if (2 * 2 == 4)
        LOG(l, "true")
    else {
        LOG(l, "false");
    }
    std::string expected = "true\n";

    REQUIRE(logs.str() == expected);
}
