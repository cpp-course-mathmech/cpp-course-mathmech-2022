#pragma once

#include <string>
#include <iostream>

class Logger {
public:
  explicit Logger(std::ostream& output_stream);

  void SetLogLine(bool value);
  void SetLogFile(bool value);

  void SpecifyFilename(const std::string& file_name);
  
  void SpecifyLine(int line);

  void Log(const std::string& message);

private:

};

#define LOG(logger, message)

