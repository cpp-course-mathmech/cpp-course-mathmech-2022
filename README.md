# Дополнительные задачи для курса C++

## Что сделать перед началом

1. Проверь, что у тебя есть доступ к [репозиторию с задачами](https://gitlab.com/cpp-course-mathmech/cpp-course-mathmech-2022)
2. Проверь, что в [ведомости](https://docs.google.com/spreadsheets/d/1rN8ZuovTR8AFiowHiqU6WrtciSbmteYyNkAouLOUIkM/) 
   есть твое имя и есть ссылка на твой репозиторий

   Если ты не нашел себя, то напиши в [чат курса](https://t.me/+XD7ICvl2q0VmMjky)
3. Проверь, что к форку есть доступ

   Если доступа нет, пиши в [чат](https://t.me/+XD7ICvl2q0VmMjky)

## Как решать задачи

1. Склонируй свой репозиторий
   
   ```
   git clone https://gitlab.com/cpp-course-mathmech/cpp-course-mathmech-2022-<username>.git
   ```
   
2. Настрой гит
   
   ```
   git config --global user.name "<your name>"
   git config --global user.email <your email>
   ```
   
3. Добавь свой репозиторий и upstream
   
   - Настрой подключение по ssh https://docs.gitlab.com/ee/user/ssh.html
   - Открой свой репозиторий в браузере 
   - Скопируй ссылку на свой репозиторий: Синяя кнопка Clone -> Clone with SSH.
   - Запусти команду из директории репозитория
   
   ```
   git remote add student "ссылка на твой репозиторий"
   git remote add upstream git@gitlab.com:cpp-course-mathmech/cpp-course-mathmech-2022.git
   ```
   
4. Настрой IDE

   Скорей всего у тебя уже все настроено, если ты проходил пояса. 
   Но если нет, то можешь выбрать то, что тебе нравится больше всего. 
   Мы рекомендуем использовать CLion.

5. Реши задачу

6. Отправь на проверку 
   - Запусти скрипт из директории с задачей
   
    ```
   cd <task_name>
   python3 ../submit.py
   ```

7. Следи за процессом тестирования со страницы CI/CD -> Pipelines своего репозитория.
   - Переходи по ссылке, которую увидишь в терминале
   - Тыкай на зеленый кружок как на картинке. Если там не зеленый кружок, а какой-то другой, все равно тыкай
   ![pipeline](pipeline.jpg "Pipeline")
   - Жми на кружок, на котором написано Grade
   - Смотри на результат тестирования
   - Проверь, что оценка появилась в [ведомости](https://docs.google.com/spreadsheets/d/1rN8ZuovTR8AFiowHiqU6WrtciSbmteYyNkAouLOUIkM)


## Как запустить тесты локально

Смотри в [TESTS_README.md](TESTS_README.md)


## Что делать, если ты уверен в решении и думаешь, что в задаче баг
   
   1. Подтяни все изменения из публичного репозитория  

      ```
      git stash
      git fetch upstream
      git pull upstream main
      git push origin main
      git stash pop
      ```
   2. Если это не помогло, сформулируй проблему и опиши ее в чате

## Баллы за баги

Если ты нашел баг в задаче, то мы поставим тебе доп. баллы. 
Сколько именно зависит от бага 

- Нашел баг – до 1 балла 
- Нашел баг и пофиксил – до 5 баллов 
- Нашел баг, пофиксил и написал тесты – до 10 баллов
