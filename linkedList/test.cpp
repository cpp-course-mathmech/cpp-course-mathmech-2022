#include <catch2/catch_all.hpp>


#include "linkedList.h"
#include <vector>
#include <chrono>


template <typename T>
std::vector<T> ToVector_(const LinkedList<T>& list) {
    std::vector<T> result;
    for (auto node = list.GetHead(); node; node = node->next) {
        result.push_back(node->value);
    }
    return result;
}

TEST_CASE("LinkedList::Public::PushFrontWorks", "") {
    LinkedList<int> list;

    list.PushFront(1);
    REQUIRE(list.GetHead()->value == 1);
    list.PushFront(2);
    REQUIRE(list.GetHead()->value == 2);
    list.PushFront(3);
    REQUIRE(list.GetHead()->value == 3);

    const std::vector<int> expected = {3, 2, 1};
    REQUIRE(ToVector_(list) == expected);
}

TEST_CASE("LinkedList::Public::InsertAfterWorks", "") {
    LinkedList<std::string> list;

    list.PushFront("a");
    auto head = list.GetHead();
    REQUIRE(head != nullptr);
    REQUIRE(head->value == "a");

    list.InsertAfter(head, "b");
    const std::vector<std::string> expected1 = {"a", "b"};
    REQUIRE(ToVector_(list) == expected1);

    list.InsertAfter(head, "c");
    const std::vector<std::string> expected2 = {"a", "c", "b"};
    REQUIRE(ToVector_(list) == expected2);
}

TEST_CASE("LinkedList::Public::RemoveAfterWorks", "") {
    LinkedList<int> list;
    for (int i = 1; i <= 5; ++i) {
        list.PushFront(i);
    }

    const std::vector<int> expected = {5, 4, 3, 2, 1};
    REQUIRE(ToVector_(list) == expected);

    auto next_to_head = list.GetHead()->next;
    list.RemoveAfter(next_to_head);
    list.RemoveAfter(next_to_head);

    const std::vector<int> expected1 = {5, 4, 1};
    REQUIRE(ToVector_(list) == expected1);

    while (list.GetHead()->next) {
        list.RemoveAfter(list.GetHead());
    }
    REQUIRE(list.GetHead()->value == 5);
}

TEST_CASE("LinkedList::Public::PopFrontWorks", "") {
    LinkedList<int> list;
    

    for (int i = 1; i <= 5; ++i) {
        list.PushFront(i);
    }
    for (int i = 1; i <= 5; ++i) {
        list.PopFront();
    }
    REQUIRE(list.GetHead() == nullptr);
}

TEST_CASE("LinkedList::Public::PushFrontPopFrontWorksFastEnough", "") {
    LinkedList<int> list;

    auto t0 = std::chrono::high_resolution_clock::now();
    for (int i = 1; i <= 500000; ++i) {
        list.PushFront(i);
    }
    for (int i = 1; i <= 500000; ++i) {
        list.PopFront();
    }
    auto t1 = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0);
    long long msCount = duration.count();
    REQUIRE(msCount < 500);
}
