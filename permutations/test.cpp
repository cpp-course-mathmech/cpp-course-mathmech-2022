#include <catch2/catch_all.hpp>


#include "permutations.h"
#include <algorithm>
#include <vector>
#include <numeric>
#include <chrono>



TEST_CASE("Permutations::Public::PermutationsWorksWhenZeroLength", "") { 
    Permutations permutation(0);
    std::vector<int> expected = {};
    REQUIRE(permutation.current() == expected);
    bool haveNext = permutation.next();
    REQUIRE(!haveNext);
}

TEST_CASE("Permutations::Public::PermutationsWorks", "") {
    Permutations permutation(3);
    std::vector<int> expected = {3, 2, 1};
    REQUIRE(permutation.current() == expected);

    for (int i = 0; i < 5; i++) {
        bool haveNext = permutation.next();
        REQUIRE(haveNext == (i != 5));
        std::prev_permutation(begin(expected), end(expected));
        REQUIRE(permutation.current() == expected);
    }
}

TEST_CASE("Permutations::Public::PermutationsComputesLazy", "")  {
    auto t0 = std::chrono::high_resolution_clock::now();
    Permutations permutation(1000);
    for (int i = 0; i < 1000000; i++) {
        permutation.next();
    }
    auto t1 = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0);
    long long msCount = duration.count();
    REQUIRE(msCount < 1000);
}


