#include <catch2/catch_all.hpp>


#include "sub.h"


TEST_CASE("SimpleTestPublic1", "") {
    REQUIRE(sub(1, 9) == -8);
}

TEST_CASE("SimpleTestPublic2", "") {
    REQUIRE(sub(3, -3) == 6);
}

