# Как запускать тесты локально

### Установи cmake

Как установить, можно посмотреть [здесь](https://cmake.org/install/) или [здесь](https://cgold.readthedocs.io/en/latest/first-step/installation.html)

### Установи Catch2

Полную докуметацию можно прочитать [здесь](https://github.com/catchorg/Catch2) 

Но в целом досточно следующего:

```
git clone https://github.com/catchorg/Catch2.git
cd Catch2
cmake -Bbuild -H. -DBUILD_TESTING=OFF
sudo cmake --build build/ --target install
```

### Как запустить тесты

В общем виде

```
cmake -S . -B ./out/build/ -DENABLE_PRIVATE_TEST=OFF -DTASK_NAME=<folder_name>
cmake --build ./out/build/
./out/build/<folder_name>/<folder_name>(.exe)
```

Пример: 
```
cmake -S . -B ./out/build/ -DENABLE_PRIVATE_TEST=OFF -DTASK_NAME=add
cmake --build ./out/build/
./out/build/add/add
```
