#include <catch2/catch_all.hpp>

#include "spendings.h"

#include <algorithm>
#include <iostream>
#include <sstream>
#include <vector>

using namespace Json;


TEST_CASE("libJson::Public::TestLoadFromJson", "") {
  std::istringstream json_input(R"([
    {"amount": 2500, "category": "apples"},
    {"amount": 1150, "category": "books"},
    {"amount": 5780, "category": "coffee"},
    {"amount": 7500, "category": "tea"},
    {"amount": 12345, "category": "tickets"},
    {"amount": 1100, "category": "flowers"}
  ])");

  const std::vector<Spending> spendings = LoadFromJson(json_input);

  const std::vector<Spending> expected = {
    {"apples", 2500},
    {"books", 1150},
    {"coffee", 5780},
    {"tea", 7500},
    {"tickets", 12345},
    {"flowers", 1100}
  };
  REQUIRE(spendings == expected);
}
