#include "token.h"


namespace LSyntax {

Token::Token() : type(TokenType::NONE_TOKEN), value("") {
    
}

Token::Token(TokenType type, std::string value, int sign)
    : type(type), value(value), sign(sign) {}
Token::~Token() {}

Token::Token(const Token& other) {
    type = other.type;
    value = other.value;
    sign = other.sign;
}

Token& Token::operator=(const Token& other) {
    type = other.type;
    value = other.value;
    sign = other.sign;
    return *this;
}

std::size_t Token::hash() const
{
    using std::size_t;
    using std::hash;
    using std::string;

    return ((hash<int>()(type)
            ^ (hash<string>()(value) << 1)) >> 1)
            ^ (hash<int>()(sign) << 1);
}

bool operator< (const Token& lhs, const Token& rhs) {
    return std::tie(lhs.type, lhs.value, lhs.sign) < std::tie(rhs.type, rhs.value, rhs.sign);
}

bool operator== (const Token& lhs, const Token& rhs) {
    return std::tie(lhs.type, lhs.value, lhs.sign) == std::tie(rhs.type, rhs.value, rhs.sign);
}

bool operator!= (const Token& lhs, const Token& rhs) {
    return !(lhs == rhs);
}

std::ostream& operator<< (std::ostream& out, const Token& token) {
    out << token.value;
    if (token.sign != 0) {
        out << token.sign;
    }
    return out;
}



Rule::Rule() {}
Rule::Rule(Token source, std::vector<Token> value, int point) :
    source(source), value(std::move(value)), point(point) {}

bool operator< (const Rule& lhs, const Rule& rhs) {
    if (lhs.source != rhs.source) {
        return lhs.source < rhs.source;
    }
    // return lhs.value < rhs.value;
    std::size_t minLen = std::min(lhs.value.size(), rhs.value.size());
    for(size_t i = 0; i < minLen; i++) {
        if (lhs.value[i] != rhs.value[i]) {
            return lhs.value[i] < rhs.value[i];
        }
    }
    if (lhs.value.size() == rhs.value.size()) {
        return lhs.point < rhs.point;
    }
    return lhs.value.size() < rhs.value.size();
}

std::ostream& operator<< (std::ostream& out, const Rule& rule) {
    out << rule.source << " -> ";
    int i = 0;
    for(const auto& token : rule.value) {
        if (rule.point == i) {
            out << ".";
        }
        out << token << " ";
        i++;
    }
    int ruleSize = rule.value.size(); 
    if (i == ruleSize) {
        out << ".";
    }
    return out;
}



Grammar::Grammar() {}

Grammar::Grammar(std::map<Token, std::set<Rule>> grammarParts)
    : rules(std::move(grammarParts)) {}

Grammar::Grammar(const Grammar& other) : rules(other.rules) {
    axiom = other.axiom;
}
Grammar& Grammar::operator=(const Grammar& other) {
    rules = other.rules;
    axiom = other.axiom;
    return *this;
}


Grammar::Grammar(Grammar&& other) : rules(std::move(other.rules)){
    axiom = other.axiom;
}
Grammar& Grammar::operator=(Grammar&& other) {
    rules = std::move(other.rules);
    axiom = other.axiom;
    return *this;
}

std::ostream& operator<< (std::ostream& out, const Grammar& grammar) {
    for(const auto& rules : grammar.rules) {
        out << rules.first << " -> ";
        for (const auto& rule : rules.second) {
            for (const auto& token : rule.value) {
                out << token << " ";
            }
            out << " | ";
        }
        out << "\n";
    }
    return out;
}

}
