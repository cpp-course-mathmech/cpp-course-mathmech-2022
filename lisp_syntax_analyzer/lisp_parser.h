#pragma once


#include <fstream>
#include <iostream>
#include <memory>


#include "lisp_lexer.h"
#include "token.h"
#include "grammar_transform.h"
#include "pda_parser.h"

#include "lisp_object.h"
#include "lisp_object_holder.h"
#include "lisp_statement.h"


void PrepareGrammar(
    LSyntax::Grammar& grammar,
    FIRST& first, FOLLOW& follow, SELECT& select, PARSING_TABLE& parsing_table);

LSyntax::Token LexTokenToToken(LLexer::LexToken lt);

void ReadProgram(
    std::istream& in,
    std::vector<LLexer::LexToken>& ltokens, std::vector<LSyntax::Token>& stokens);

void ReadProgramFromFile(
    std::string fname,
    std::vector<LLexer::LexToken>& ltokens, std::vector<LSyntax::Token>& stokens);

enum StackFrameType {
    StackFrameType_None,
    StackFrameType_DEFVAR,
    StackFrameType_DEFUN,
    StackFrameType_FUNCALL,
    StackFrameType_LAMBDA,
    StackFrameType_QUOTE,
    StackFrameType_ID_LIST,
    StackFrameType_PRINT,
    StackFrameType_LIST,
    StackFrameType_RETURN,
    StackFrameType_IF,
};

struct StackFrame {
    StackFrameType type = StackFrameType::StackFrameType_None;
    std::string name;
    std::vector<std::string> names;
    std::vector<std::unique_ptr<LAst::LStatement>> statements;
    int counter = -1;
};


std::unique_ptr<LAst::LStatement> ConvertStackFrameToStatement(StackFrame sf);

void ReduceForm(std::vector<StackFrame>& stack);

std::vector<std::unique_ptr<LAst::LStatement>> BuildAst(
    PDA_Parser pdaParser, const std::vector<LLexer::LexToken>& ltokens);

void AddBuiltIns(LRuntime::Closure& closure);

void Execute(std::vector<std::unique_ptr<LAst::LStatement>> statements);
