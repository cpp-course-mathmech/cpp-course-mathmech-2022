#pragma once

#include <set>
#include <map>
#include <vector>
#include <utility>

#include "token.h"
#include "trie.h"

LSyntax::Grammar MakeLambdaFree(const LSyntax::Grammar& grammar);


std::set<LSyntax::Token> FindAndRemoveDirectLambdaNonTerminals(
        std::set<LSyntax::Token> sourcesToCheck,
        LSyntax::Grammar& grammar,
        std::set<LSyntax::Token>& directLambdaNonTerminals);

std::set<LSyntax::Token> FindLambdaNonTerminals(
        std::set<LSyntax::Token> sourcesToCheck,
        const LSyntax::Grammar& grammar,
        std::set<LSyntax::Token>& lambdaNonTerminals);

bool CheckAllLambdaTokens(const LSyntax::Rule& rule, const std::set<LSyntax::Token>& lambdaTokens);

void PrintTrieNode(std::ostream& out, const CTrieNode<LSyntax::Token, LSyntax::Rule>& node, int level = 0);

std::set<LSyntax::Rule> MakeRuleReduction(const LSyntax::Rule& rule, const std::set<LSyntax::Token>& lambdaTokens);





LSyntax::Token FactorizeNode(
    LSyntax::Token source,
    std::shared_ptr<CTrieNode<LSyntax::Token, LSyntax::Rule>> trie_node,
    LSyntax::Grammar& grammar, int& source_counter,
    std::map<std::string, int>& term_name_to_max_sign);

LSyntax::Grammar LeftFactorize(const LSyntax::Grammar& grammar);

void Unwrap(
    LSyntax::Rule rule_to_expand,
    LSyntax::Token to_unwrap,
    const LSyntax::Grammar& grammar,
    std::set<LSyntax::Rule>& new_rules);

void EliminateDirectLeftRecursion(LSyntax::Token source, LSyntax::Grammar& grammar, std::map<std::string, int>& term_name_to_max_sign);

std::map<std::string, int> FindMaxSigns(const LSyntax::Grammar& grammar);

bool CheckLeftRecursion(LSyntax::Token source, LSyntax::Grammar& grammar);


LSyntax::Grammar EliminateLeftRecursion(const LSyntax::Grammar& grammar);


using FIRST = typename std::map<LSyntax::Token, std::set<LSyntax::Token>>;
using FOLLOW = typename std::map<LSyntax::Token, std::set<LSyntax::Token>>;
using SELECT = typename std::map<LSyntax::Rule, std::set<LSyntax::Token>>;

bool CalcFirstForRule(const LSyntax::Rule& rule, FIRST& first);

void PrintFirst(std::ostream& out, FIRST& first);

void PrintSelect(std::ostream& out, SELECT& select);

void SetFirstOfTerminals(const LSyntax::Grammar& grammar, FIRST& first);

// FIRST
std::map<LSyntax::Token, std::set<LSyntax::Token>> CalcFirst(const LSyntax::Grammar& grammar);

bool CalcFollowForRule(const LSyntax::Rule& rule, FOLLOW& follow, const FIRST& first);

// FOLLOW
FOLLOW CalcFollow(const LSyntax::Grammar& grammar, const FIRST& first);


void CalcSelect(const LSyntax::Rule& rule, const FIRST& first, const FOLLOW& follow, SELECT& select);

// SELECT
SELECT CalcSelect(const LSyntax::Grammar& grammar, const FIRST& first, const FOLLOW& follow);

// LL(1) Check
bool IsLL1(const LSyntax::Grammar& grammar, const SELECT& select);

// PARSING TABLE
using PARSING_TABLE = typename std::map<LSyntax::Token, std::map<LSyntax::Token, LSyntax::Rule>>;

void PrintParsingTable(std::ostream& out, PARSING_TABLE& parse_table);



PARSING_TABLE CalcParsingTable(const LSyntax::Grammar& grammar, const SELECT& select);

