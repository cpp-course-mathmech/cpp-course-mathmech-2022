#pragma once

#include <vector>
#include <unordered_map>
#include <map>
#include <memory>
#include <ostream>


template<typename Char, typename Data>
struct CTrieNode {
    Data value;
    std::vector<Char> part;
    std::map<Char, std::shared_ptr<CTrieNode<Char, Data>>> nodes;
    bool is_root = false;
    CTrieNode() {
        is_root = true;
    }
    CTrieNode(Data value) : value(value) {}

    bool operator== (const CTrieNode<Char, Data>& other) {
        return value == other.value && part == other.part;
    }

    bool operator< (const CTrieNode<Char, Data>& other) {
        return value < other.value;
    }
};


template<typename Char, typename Data>
class CTrie {
public:
    std::shared_ptr<CTrieNode<Char, Data>> root_node = std::make_shared<CTrieNode<Char, Data>>();

    CTrie() {}

    void Add(Data data, const std::vector<Char>& str) {
        std::shared_ptr<CTrieNode<Char, Data>> curr_node = root_node;

        size_t curr_node_part_idx = 0;
        for (size_t i = 0; i < str.size(); ++i) {
            if (curr_node_part_idx < curr_node->part.size()) {
                if (curr_node->part[curr_node_part_idx] == str[i]) {
                    curr_node_part_idx++;
                    continue;
                } else {
                    InsertAt(data, str, i, curr_node, curr_node_part_idx);
                    break;
                }
            } else if (curr_node->nodes.find(str[i]) != curr_node->nodes.end()) {
                curr_node = curr_node->nodes[str[i]];
                curr_node_part_idx = 0;
            } else {
                InsertNext(data, str, i, curr_node);
                break;
            }
        }
    }

    void InsertNext(Data data, const std::vector<Char>& str, size_t idx_from, std::shared_ptr<CTrieNode<Char, Data>> node_from) {
        std::shared_ptr<CTrieNode<Char, Data>> new_node(new CTrieNode<Char, Data>(data));
        for(size_t i = idx_from + 1; i < str.size(); ++i) {
            new_node->part.push_back(str[i]);
        }
        node_from->nodes[str[idx_from]] = new_node;
    }

    void InsertAt(Data data, const std::vector<Char>& str, size_t str_idx, std::shared_ptr<CTrieNode<Char, Data>> node, size_t part_idx) {
        std::shared_ptr<CTrieNode<Char, Data>> node_shard(new CTrieNode<Char, Data>(data));
        node_shard->nodes = std::move(node->nodes);
        Char char_to_shard = node->part[part_idx];
        for (size_t i = part_idx + 1; i < node->part.size(); i++) {
            node_shard->part.push_back(node->part[i]);
        }
        node->part.erase(node->part.begin() + part_idx, node->part.end());
        node->nodes[char_to_shard] = node_shard;
        InsertNext(data, str, str_idx, node);
    }
};
