#pragma once

#include <iostream>
#include <ostream>
#include <string>
#include <vector>
#include <tuple>
#include <unordered_map>
#include <set>
#include <map>
#include <memory>

namespace LSyntax {

enum TokenType {
    NONE_TOKEN,
    TERMINAL,
    NON_TERMINAL
};

class Token {
public:
    TokenType type;
    std::string value;
    int sign = 0;

    Token();

    Token(TokenType type, std::string value, int sign = 0);
    virtual ~Token();

    Token(const Token& other);

    Token& operator=(const Token& other);

    std::size_t hash() const;
};

bool operator< (const Token& lhs, const Token& rhs);

bool operator== (const Token& lhs, const Token& rhs);

bool operator!= (const Token& lhs, const Token& rhs);

std::ostream& operator<< (std::ostream& out, const Token& token);



class Rule {
public:
    Token source;
    std::vector<Token> value;
    int point = 0;

    Rule();
    Rule(Token source, std::vector<Token> value, int point = 0);
};

bool operator< (const Rule& lhs, const Rule& rhs);

std::ostream& operator<< (std::ostream& out, const Rule& rule);



class Grammar {
public:
    Token axiom = Token(TokenType::NON_TERMINAL, "S");
    std::map<Token, std::set<Rule>> rules;

    Grammar();
    Grammar(std::map<Token, std::set<Rule>> grammarParts);
    Grammar(const Grammar& other);
    Grammar& operator=(const Grammar& other);
    Grammar(Grammar&& other);
    Grammar& operator=(Grammar&& other);
};

std::ostream& operator<< (std::ostream& out, const Grammar& grammar);

static const Token Lambda(TokenType::TERMINAL, "Lambda");
static const Token EndOfStr(TokenType::TERMINAL, "$");

}

