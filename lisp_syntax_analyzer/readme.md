# Задача Syntax Analyzer

## Нужно реализовать недостающие части
### Реализуйте методы и функции, помеченные 
```
// TODO: Implement
```
#### Файлы требующие доработки
1) lisp_object_holder.cpp - класс для хранения произвольных объектов
    - IsTrue
    - EqualComparator
    - LessComparator
2) lisp_object.cpp - методы для получения текстового представления
    - Bool::Print
    - LFunction::Print
3) lisp_parser.cpp - чтение программы и добавление встроенных функций
    - ReadProgram
    - AddBuiltIns
4) lisp_statements.cpp - реализовать методы Execute для выполнения узла AST дерева для классов:
    - DefunStatement
    - FuncCallStatement
    - DefvarStatement
    - ListStatement
    - Print
    - IfStatement
    - Or
    - And
    - Not
    - Add
    - Sub
    - Mult
    - Div
    - Comparison
    - Nth
    - Cons
    - Rest
    - Sort
    - Car
    - MapList
    - QuoteStatement



## Преобразования грамматики (уже реализованы)
## Задать грамматику с помощью token.h
### Теория:
#### Грамматика - это четверка:
1) Множество терминалов
2) Множество нетерминалов
3) Аксиома
4) Правила вывода 

### В ```коде```:
```
Token E(TokenType::NON_TERMINAL, "E");
Token T(TokenType::NON_TERMINAL, "T");
Token F(TokenType::NON_TERMINAL, "F");
Token plus(TokenType::TERMINAL, "+");
Token minus(TokenType::TERMINAL, "-");
Token mult(TokenType::TERMINAL, "*");
Token divide(TokenType::TERMINAL, "/");
Token id(TokenType::TERMINAL, "id");
Token lpar(TokenType::TERMINAL, "(");
Token rpar(TokenType::TERMINAL, ")");

std::map<Token, std::set<Rule>> grammarParts = {
    {E, { Rule(E, {E, plus, T}), Rule(E, {E, minus, T}), Rule(E, {T})}},
    {T, { Rule(T, {T, mult, F}), Rule(T, {T, divide, F}), Rule(T, {F})}},
    {F, { Rule(F, {lpar, E, rpar}), Rule(F, {id})}},
};
```

## Избавитьс от левой рекурсии
### Теория:
1) Левая рекурсия может быть непосредственной:
```
A -> Aac
```
2) Также может присутствовать неявно
```
A -> Bc
B -> Aa
Можем заменить нетерминал B на правую часть и получить:
A -> Aac
```

3) Избавиться от непосредственной левой рекурсии:
```
Правило
A -> Aa1 | Aa2 | ... | Aan | b1 | ... | bn
Заменим на
A' -> a1A' | a2A' | ... | anA'
A -> b1A' | ... | bnA' 
```
4) Избавиться от левой рекурсии
```
Зададим порядок на множестве нетерминалов
A < B < C ...
Пока грамматика не стабилизировалась
    Для каждого правила A -> Ba...
        Если B < A:
            Заменим это правило на правила образованные
            заменой B на его правую часть
    Если появилась непосредственная левая рекурсия,
    избавиться от нее 
```
### В ```коде```:
```
nonrecursive_grammar = EliminateLeftRecursion(grammar);
```
<hr/>

## Провести левую факторизацию
1) Рассмотрим правила с левой частью A
```
A -> ab
A -> ac
найдем общий префикс (a)
Заменим правила с общим префиксом на
A -> aA'
A -> b | c
```
2) Левая факторизация
```
Пока грамматика не стабилизировалась
    Для правил с общей левой части найдем наибольший общий префикс: A -> ab | ac | ...
        Заменим эти правила на:
            A -> aA'
            A -> b | c | ...
```
### В ```коде```:
```
new_grammar = LeftFactorize(nonrecursive_grammar);
```
<hr/>

## Найти множества FIRST и FOLLOW для модифицированной грамматики
### Теория:
```
FIRST(A) = {терминалы, на которые может начинаться строка полученная из символа A}
FOLLOW(A) = {терминалы, которые могут следовать за строкой, полученной из символа А}
```

#### Найдем множество FIRST
```
Пусть есть правило A -> X1 X2 X3 ...
FIRST(A) <- FIRST(X1) \ {lambda}
if lambda in FIRST(X1):
    FIRST(A) <- FIRST(X2) \ {lambda}
    if lambda in FIRST(X2):
        FIRST(A) <- FIRST(X3) \ {lambda}
            ...
            if lambda in FIRST(Xn):
                FIRST(A) <- lambda
```
#### Найдем множество FOLLOW
```
Пусть есть правило A -> X1 ... Xn-1 Xn

FOLLOW(Xn) <- FOLLOW(A) но мы пока не посчитали FOLLOW(A) и оно пусто
for(i = n-1; i >= 0; i++):
    FOLLOW(Xi) <- FIRST(Xi+1...Xn) \ {lambda}
    if lambda in FIRST(Xi+1...Xn):
        FOLLOW(Xi) <- FOLLOW(A)

```

### В ```коде```:
```
FIRST first = CalcFirst(new_grammar);
FOLLOW follow = CalcFollow(new_grammar, first);
```

## Найти множество SELECT
### Теория:
```
SELECT(A -> alpha) = FIRST(alpha) if lambda not in FIRST(alpha)
    else FIRST(alpha) / {lambda} + FOLLOW(A)
```
### В ```коде```:
```
SELECT select = CalcSelect(new_grammar, first, follow);
```

## Проверить, что грамматика LL1
- Нужно для Predictive парсера
- Если не LL1 изменить исходную грамматику
#### Проверяем что множества SELECT для правил с одинаковой левой частью не пересекаются
#### LL1:
- L - Читаем слева направо
- L - Раскраваем самый левый нетерминал
- 1 - Требуется знать только один текущий символ строки, чтобы понять какое правило применить для МПА (автомата с магазинной памятью)
```
bool isLL1 = IsLL1(new_grammar, select);
```

## Сгенерировать PARSING_TABLE
#### Используя множество SELECT строим таблицу:
- Строка - Нетерминал на вершине стека
- Столбец - терминал в строке переданной автомату
- Значение ячейки - правая часть правила, которое применяем
```
PARSING_TABLE parse_table = CalcParsingTable(new_grammar, select);
```

## Запустить Nonrecursive Predictive Parser
- Можно подавать токены итеративно
- Можно добавить генерацию кода
- Можно добавить валидацию синхронизирующими правилами (Panic Mode) (пока не реализовано)
### МПА, управляемый ParsingTable (управляющей таблицей)
- Есть 3 вида правила
    1) Принимающее правило: стек пуст (не считая символа конца стека), строка обработана до конца  
    2) Терминал на вершине стека и очередной символ строки совпадают, тогда перемещаем указатель строки и выбрасываем вершину стека
    3) На вершине стека нетерминал A, очередной символ строки b
        - Берем правую часть правила из ячейки из сроки A и столбцем b в ParsingTable
        - Оставляем указатель строки на месте
        - Выбрасываем вершину стека
        - Вставляем полученную из таблицы последовательность в стек
- Во время применения второго правила можно понять, какой терминал в какую последовательность раскрылся

```
// id + id * id
//          S
//    T           E1
//  id  T1      +   T       E1
//      e           id    T1        e
//                      *   F   T1
//                          id    e

std::vector<gehn::Token> input = {
    id, plus, id, mult, id, gehn::EndOfStr
};
ParsePredictive(input, new_grammar, parse_table);
```
