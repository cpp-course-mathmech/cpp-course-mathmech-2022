#include "lisp_statement.h"
#include "lisp_object.h"

#include <iostream>
#include <sstream>
#include <memory>

using namespace std;

namespace LAst {

using LRuntime::Closure;




VarValue::VarValue(std::string name)
  : name(name) {}

ObjectHolder VarValue::Execute(LRuntime::Closure& closure) {
  return closure.at(name);
}


ObjectHolder Return::Execute(LRuntime::Closure& closure) {
    return statement->Execute(closure);
}


DefunStatement::DefunStatement(
  std::string name,
  std::vector<std::string> argument_names,
  std::vector<std::unique_ptr<LStatement>> body
) : name(name), argument_names(std::move(argument_names)),
    body(std::move(body)) {}

ObjectHolder DefunStatement::Execute(LRuntime::Closure& closure) {
  // TODO: Implement
  return ObjectHolder::None();
}

FuncCallStatement::FuncCallStatement(
  std::string name,
  std::vector<std::unique_ptr<LStatement>> args
) : name(name), args(std::move(args)) {}

ObjectHolder FuncCallStatement::Execute(LRuntime::Closure& closure) {
  // TODO: Implement
  return ObjectHolder::None();
}


DefvarStatement::DefvarStatement(
  std::string name,
  std::unique_ptr<LStatement> body
) : name(name), body(std::move(body)) {}

ObjectHolder DefvarStatement::Execute(LRuntime::Closure& closure) {
  // TODO: Implement
  return ObjectHolder::None();
}

ListStatement::ListStatement(
  std::vector<std::unique_ptr<LStatement>> args
) : args(std::move(args)) {

}

ObjectHolder ListStatement::Execute(LRuntime::Closure& closure) {
  // TODO: Implement
  return ObjectHolder::None();
}


Print::Print(unique_ptr<LStatement> argument_)
{
   argument = std::move(argument_);
}

ObjectHolder Print::Execute(Closure& closure) {
  // TODO: Implement
  return ObjectHolder();
}

ostream* Print::output = &cout;

void Print::SetOutputStream(ostream& output_stream) {
  output = &output_stream;
}


IfStatement::IfStatement(
  std::unique_ptr<LStatement> condition,
  std::unique_ptr<LStatement> if_body,
  std::unique_ptr<LStatement> else_body
) : condition(std::move(condition)), if_body(std::move(if_body)), else_body(std::move(else_body))
{
}

ObjectHolder IfStatement::Execute(LRuntime::Closure& closure) {
  // TODO: Implement
  return ObjectHolder::None();
}

ObjectHolder Or::Execute(LRuntime::Closure& closure) {
  // TODO: Implement
  return ObjectHolder::None();
}

ObjectHolder And::Execute(LRuntime::Closure& closure) {
  // TODO: Implement
  return ObjectHolder::None();
}

ObjectHolder Not::Execute(LRuntime::Closure& closure) {
  // TODO: Implement
  return ObjectHolder::None();
}

ObjectHolder Add::Execute(Closure& closure) {
  // TODO: Implement
  return ObjectHolder::None();
}

ObjectHolder Sub::Execute(Closure& closure) {
  // TODO: Implement
  return ObjectHolder::None();
}

ObjectHolder Mult::Execute(LRuntime::Closure& closure) {
  // TODO: Implement
  return ObjectHolder::None();
}

ObjectHolder Div::Execute(LRuntime::Closure& closure) {
  // TODO: Implement
  return ObjectHolder::None();
}

Comparison::Comparison(
  Comparator cmp,
  std::unique_ptr<LStatement> lhs,
  std::unique_ptr<LStatement> rhs
) : cmp(cmp), lhs(std::move(lhs)), rhs(std::move(rhs)) {}

ObjectHolder Comparison::Execute(LRuntime::Closure& closure) {
  // TODO: Implement
  return ObjectHolder::None();
}




ObjectHolder Nth::Execute(LRuntime::Closure& closure) {
  // TODO: Implement
  return ObjectHolder::None();
}

ObjectHolder Cons::Execute(LRuntime::Closure& closure) {
  // TODO: Implement
  return ObjectHolder::None();
}

ObjectHolder Rest::Execute(LRuntime::Closure& closure) {
  // TODO: Implement
  return ObjectHolder::None();
}

ObjectHolder Sort::Execute(LRuntime::Closure& closure) {
  // TODO: Implement
  return ObjectHolder::None();
}

ObjectHolder Car::Execute(LRuntime::Closure& closure) {
  // TODO: Implement
  return ObjectHolder::None();
}


ObjectHolder MapList::Execute(LRuntime::Closure& closure) {
  // TODO: Implement
  return ObjectHolder::None();
}



QuoteStatement::QuoteStatement(std::string name)
  : name(std::move(name)) {}

QuoteStatement::QuoteStatement(std::unique_ptr<LStatement> body)
  : body(std::move(body)) {}


ObjectHolder QuoteStatement::Execute(LRuntime::Closure& closure) {
  // TODO: Implement
  return ObjectHolder::None();
}

}