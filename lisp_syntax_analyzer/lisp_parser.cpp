#include "lisp_parser.h"



LSyntax::Token program(LSyntax::TokenType::NON_TERMINAL, "program");
LSyntax::Token form(LSyntax::TokenType::NON_TERMINAL, "form");
LSyntax::Token form_expr(LSyntax::TokenType::NON_TERMINAL, "form_expr");
LSyntax::Token defvar_expr(LSyntax::TokenType::NON_TERMINAL, "defvar_expr");
LSyntax::Token defparam_expr(LSyntax::TokenType::NON_TERMINAL, "defparam_expr");
LSyntax::Token defun_expr(LSyntax::TokenType::NON_TERMINAL, "defun_expr");
LSyntax::Token defmacro_expr(LSyntax::TokenType::NON_TERMINAL, "defmacro_expr");
LSyntax::Token defstruct_expr(LSyntax::TokenType::NON_TERMINAL, "defstruct_expr");
LSyntax::Token defclass_expr(LSyntax::TokenType::NON_TERMINAL, "defclass_expr");
LSyntax::Token defgeneric_expr(LSyntax::TokenType::NON_TERMINAL, "defgeneric_expr");
LSyntax::Token defmethod_expr(LSyntax::TokenType::NON_TERMINAL, "defmethod_expr");
LSyntax::Token slot_value_expr(LSyntax::TokenType::NON_TERMINAL, "slot_value_expr");
LSyntax::Token lambda_expr(LSyntax::TokenType::NON_TERMINAL, "lambda_expr");
LSyntax::Token return_expr(LSyntax::TokenType::NON_TERMINAL, "return_expr");
LSyntax::Token set_expr(LSyntax::TokenType::NON_TERMINAL, "set_expr");
LSyntax::Token setq_expr(LSyntax::TokenType::NON_TERMINAL, "setq_expr");
LSyntax::Token setf_expr(LSyntax::TokenType::NON_TERMINAL, "setf_expr");
LSyntax::Token let_expr(LSyntax::TokenType::NON_TERMINAL, "let_expr");
LSyntax::Token print_expr(LSyntax::TokenType::NON_TERMINAL, "print_expr");
LSyntax::Token list_expr(LSyntax::TokenType::NON_TERMINAL, "list_expr");
LSyntax::Token if_expr(LSyntax::TokenType::NON_TERMINAL, "if_expr");
LSyntax::Token when_expr(LSyntax::TokenType::NON_TERMINAL, "when_expr");
LSyntax::Token unless_expr(LSyntax::TokenType::NON_TERMINAL, "unless_expr");
LSyntax::Token id_expr(LSyntax::TokenType::NON_TERMINAL, "id_expr");
LSyntax::Token form_call_expr(LSyntax::TokenType::NON_TERMINAL, "form_call_expr");

LSyntax::Token form_params(LSyntax::TokenType::NON_TERMINAL, "form_params");
LSyntax::Token form_param(LSyntax::TokenType::NON_TERMINAL, "form_param");
LSyntax::Token IDs(LSyntax::TokenType::NON_TERMINAL, "IDs");


LSyntax::Token OP_term(LSyntax::TokenType::TERMINAL, "(");
LSyntax::Token CP_term(LSyntax::TokenType::TERMINAL, ")");
LSyntax::Token ID_term(LSyntax::TokenType::TERMINAL, "ID");
LSyntax::Token STRING_term(LSyntax::TokenType::TERMINAL, "STRING");
LSyntax::Token NUMBER_term(LSyntax::TokenType::TERMINAL, "NUMBER");
LSyntax::Token QUOTE_term(LSyntax::TokenType::TERMINAL, "QUOTE");
LSyntax::Token SHARP_term(LSyntax::TokenType::TERMINAL, "SHARP");
LSyntax::Token T_term(LSyntax::TokenType::TERMINAL, "T");
LSyntax::Token NIL_term(LSyntax::TokenType::TERMINAL, "NIL");
LSyntax::Token KEYWORD_term(LSyntax::TokenType::TERMINAL, "KEYWORD");

LSyntax::Token DEFVAR_term(LSyntax::TokenType::TERMINAL, "DEFVAR");
LSyntax::Token DEFPARAM_term(LSyntax::TokenType::TERMINAL, "DEFPARAM");
LSyntax::Token DEFUN_term(LSyntax::TokenType::TERMINAL, "DEFUN");
LSyntax::Token DEFMACRO_term(LSyntax::TokenType::TERMINAL, "DEFMACRO");
LSyntax::Token DEFSTRUCT_term(LSyntax::TokenType::TERMINAL, "DEFSTRUCT");
LSyntax::Token DEFCLASS_term(LSyntax::TokenType::TERMINAL, "DEFCLASS");
LSyntax::Token LAMBDA_term(LSyntax::TokenType::TERMINAL, "LAMBDA");
LSyntax::Token RETURN_term(LSyntax::TokenType::TERMINAL, "RETURN");
LSyntax::Token SET_term(LSyntax::TokenType::TERMINAL, "SET");
LSyntax::Token SETQ_term(LSyntax::TokenType::TERMINAL, "SETQ");
LSyntax::Token SETF_term(LSyntax::TokenType::TERMINAL, "SETF");
LSyntax::Token LET_term(LSyntax::TokenType::TERMINAL, "LET");
LSyntax::Token PRINT_term(LSyntax::TokenType::TERMINAL, "PRINT");
LSyntax::Token LIST_term(LSyntax::TokenType::TERMINAL, "LIST");
LSyntax::Token IF_term(LSyntax::TokenType::TERMINAL, "IF");
LSyntax::Token WHEN_term(LSyntax::TokenType::TERMINAL, "WHEN");
LSyntax::Token UNLESS_term(LSyntax::TokenType::TERMINAL, "UNLESS");
LSyntax::Token SLOT_VALUE_term(LSyntax::TokenType::TERMINAL, "SLOT_VALUE");



void PrepareGrammar(
    LSyntax::Grammar& grammar, FIRST& first, FOLLOW& follow,
    SELECT& select, PARSING_TABLE& parsing_table) {

    std::map<LSyntax::Token, std::set<LSyntax::Rule>> grammarParts = {
        { program, {
                LSyntax::Rule(program, {form, program}),
                LSyntax::Rule(program, {LSyntax::Lambda})
            }
        },
        { form, {
                LSyntax::Rule(form, {OP_term, form_expr, CP_term}),
            }
        },
        { form_expr, {
                LSyntax::Rule(form_expr, {defvar_expr}),
                LSyntax::Rule(form_expr, {defparam_expr}),
                LSyntax::Rule(form_expr, {defun_expr}),
                LSyntax::Rule(form_expr, {defstruct_expr}),
                LSyntax::Rule(form_expr, {slot_value_expr}),
                LSyntax::Rule(form_expr, {lambda_expr}),
                LSyntax::Rule(form_expr, {return_expr}),
                LSyntax::Rule(form_expr, {set_expr}),
                LSyntax::Rule(form_expr, {setq_expr}),
                LSyntax::Rule(form_expr, {setf_expr}),
                LSyntax::Rule(form_expr, {let_expr}),
                LSyntax::Rule(form_expr, {print_expr}),
                LSyntax::Rule(form_expr, {list_expr}),
                LSyntax::Rule(form_expr, {if_expr}),
                LSyntax::Rule(form_expr, {when_expr}),
                LSyntax::Rule(form_expr, {unless_expr}),
                LSyntax::Rule(form_expr, {id_expr}),
                LSyntax::Rule(form_expr, {form_call_expr}),
            }
        },
        { form_params, {
                LSyntax::Rule(form_params, {form_param, form_params}),
                LSyntax::Rule(form_params, {LSyntax::Lambda}),
            }
        },
        { form_param, {
                LSyntax::Rule(form_param, {ID_term}),
                LSyntax::Rule(form_param, {STRING_term}),
                LSyntax::Rule(form_param, {NUMBER_term}),
                LSyntax::Rule(form_param, {QUOTE_term, ID_term}),
                LSyntax::Rule(form_param, {SHARP_term, QUOTE_term, ID_term}),
                LSyntax::Rule(form_param, {T_term}),
                LSyntax::Rule(form_param, {NIL_term}),
                LSyntax::Rule(form_param, {KEYWORD_term}),
                LSyntax::Rule(form_param, {form}),
            }
        },
        { IDs, {
                LSyntax::Rule(IDs, {ID_term, IDs}),
                LSyntax::Rule(IDs, {LSyntax::Lambda}),
            }
        },
        { defvar_expr, {
                LSyntax::Rule(defvar_expr, {DEFVAR_term, ID_term, form_param})
            }
        },
        { defparam_expr, {
                LSyntax::Rule(defparam_expr, {DEFPARAM_term, ID_term, form_param})
            }
        },
        { defun_expr, {
                LSyntax::Rule(defun_expr, {DEFUN_term, ID_term, OP_term, IDs, CP_term, program})
            }
        },
        { defstruct_expr, {
                LSyntax::Rule(defstruct_expr, {DEFSTRUCT_term, ID_term, IDs})
            }
        },
        { slot_value_expr, {
                LSyntax::Rule(slot_value_expr, {SLOT_VALUE_term, ID_term, ID_term})
            }
        },
        { lambda_expr, {
                LSyntax::Rule(lambda_expr, {LAMBDA_term, OP_term, IDs, CP_term, program})
            }
        },
        { return_expr, {
                LSyntax::Rule(return_expr, {RETURN_term, form_param})
            }
        },
        { set_expr, {
                LSyntax::Rule(set_expr, {SET_term, form_param})
            }
        },
        { setq_expr, {
                LSyntax::Rule(setq_expr, {SETQ_term, form_param})
            }
        },
        { setf_expr, {
                LSyntax::Rule(setf_expr, {SETF_term, form_param})
            }
        },
        { let_expr, {
                LSyntax::Rule(let_expr, {LET_term, OP_term, program, CP_term, program})
            }
        },
        { print_expr, {
                LSyntax::Rule(print_expr, {PRINT_term, form_param})
            }
        },
        { list_expr, {
                LSyntax::Rule(list_expr, {LIST_term, form_params})
            }
        },
        { if_expr, {
                LSyntax::Rule(if_expr, {IF_term, form_param, form_param, form_param})
            }
        },
        { when_expr, {
                LSyntax::Rule(when_expr, {WHEN_term, form_param, program})
            }
        },
        { unless_expr, {
                LSyntax::Rule(unless_expr, {UNLESS_term, form_param, program})
            }
        },
        { id_expr, {
                LSyntax::Rule(id_expr, {ID_term, form_params})
            }
        },
        { form_call_expr, {
                LSyntax::Rule(form_call_expr, {form, form_params})
            }
        },
    };

    grammar.rules = grammarParts;
    grammar.axiom = program;

    grammar = LeftFactorize(grammar);

    grammar = EliminateLeftRecursion(grammar);

    first = CalcFirst(grammar);

    follow = CalcFollow(grammar, first);

    select = CalcSelect(grammar, first, follow);

    parsing_table = CalcParsingTable(grammar, select);
}

std::map<LLexer::LexTokenType, LSyntax::Token> typeToToken = {
    {LLexer::LexTokenType::LEX_OP, OP_term},
    {LLexer::LexTokenType::LEX_CP, CP_term},
    {LLexer::LexTokenType::LEX_ID, ID_term},
    {LLexer::LexTokenType::LEX_KEYWORD, KEYWORD_term},
    {LLexer::LexTokenType::LEX_NUMBER, NUMBER_term},
    {LLexer::LexTokenType::LEX_LITERAL, STRING_term},
    {LLexer::LexTokenType::LEX_T, T_term},
    {LLexer::LexTokenType::LEX_NIL, NIL_term},
    {LLexer::LexTokenType::LEX_IF, IF_term},
    {LLexer::LexTokenType::LEX_WHEN, IF_term},
    {LLexer::LexTokenType::LEX_UNLESS, IF_term},
    {LLexer::LexTokenType::LEX_COND, IF_term},
    {LLexer::LexTokenType::LEX_LOOP, IF_term},
    {LLexer::LexTokenType::LEX_PRINT, PRINT_term},
    {LLexer::LexTokenType::LEX_SET, SET_term},
    {LLexer::LexTokenType::LEX_SETQ, SETQ_term},
    {LLexer::LexTokenType::LEX_SETF, SETF_term},
    {LLexer::LexTokenType::LEX_LET, LET_term},
    {LLexer::LexTokenType::LEX_LAMBDA, LAMBDA_term},
    {LLexer::LexTokenType::LEX_DEFUN, DEFUN_term},
    {LLexer::LexTokenType::LEX_DEFVAR, DEFVAR_term},
    {LLexer::LexTokenType::LEX_DEFPARAMETER, DEFPARAM_term},
    {LLexer::LexTokenType::LEX_DEFMACRO, DEFMACRO_term},
    {LLexer::LexTokenType::LEX_DEFCLASS, DEFCLASS_term},
    {LLexer::LexTokenType::LEX_DEFSTRUCT, DEFSTRUCT_term},
    {LLexer::LexTokenType::LEX_PRINT, PRINT_term},
    {LLexer::LexTokenType::LEX_LIST, LIST_term},
    {LLexer::LexTokenType::LEX_RETURN, RETURN_term},
    {LLexer::LexTokenType::LEX_QUOTE, QUOTE_term},
    {LLexer::LexTokenType::LEX_SHARP, SHARP_term},
};


LSyntax::Token LexTokenToToken(LLexer::LexToken lt) {
    if (typeToToken.find(lt.type) != typeToToken.end()) {
        return typeToToken.at(lt.type);
    }
    return ID_term;
}


void ReadProgram(
    std::istream& in,
    std::vector<LLexer::LexToken>& ltokens, std::vector<LSyntax::Token>& stokens) {
    // TODO: Implement

}

void ReadProgramFromFile(
    std::string fname,
    std::vector<LLexer::LexToken>& ltokens, std::vector<LSyntax::Token>& stokens) {
    
    std::ifstream file(fname);
    ReadProgram(file, ltokens, stokens);
    file.close();
}



std::unique_ptr<LAst::LStatement> ConvertStackFrameToStatement(StackFrame sf) {
    if (sf.type == StackFrameType::StackFrameType_DEFVAR) {
        if (sf.statements.size() > 0) {
            return std::make_unique<LAst::DefvarStatement>(
                sf.name, std::move(sf.statements[0]));
        } else {
            std::cout << "ERROR::stack is empty" << std::endl;
        }
    } else if (sf.type == StackFrameType::StackFrameType_DEFUN) {
        return std::make_unique<LAst::DefunStatement>(
            sf.name, sf.names, std::move(sf.statements));
    } else if (sf.type == StackFrameType::StackFrameType_FUNCALL) {
        return std::make_unique<LAst::FuncCallStatement>(
            sf.name, std::move(sf.statements)
        );
    } else if (sf.type == StackFrameType::StackFrameType_QUOTE) {
        if (!sf.name.empty()) {
            return std::make_unique<LAst::QuoteStatement>(
                sf.name
            );
        } else if (!sf.statements.empty()) {
            return std::make_unique<LAst::QuoteStatement>(
                std::move(sf.statements[0])
            );
        }
        std::cout << "ConvertStackFrameToStatement::QUOTE::ERROR: no argument" << std::endl;
    } else if (sf.type == StackFrameType::StackFrameType_LAMBDA) {
        return std::make_unique<LAst::DefunStatement>(
            "", sf.names, std::move(sf.statements));
    }else if (sf.type == StackFrameType::StackFrameType_PRINT) {
        return std::make_unique<LAst::Print>(
            std::move(sf.statements[0])
        );
    } else if (sf.type == StackFrameType::StackFrameType_LIST) {
        return std::make_unique<LAst::ListStatement>(
            std::move(sf.statements)
        );
    } else if (sf.type == StackFrameType::StackFrameType_RETURN) {
        if (sf.statements.size() > 0) {
            return std::make_unique<LAst::Return>(std::move(sf.statements[0]));
        } else {
            std::cout << "ERROR::stack is empty" << std::endl;
        }
    } else if (sf.type == StackFrameType::StackFrameType_IF) {
        if (sf.statements.size() == 3) {
            return std::make_unique<LAst::IfStatement>(
                std::move(sf.statements[0]),
                std::move(sf.statements[1]),
                std::move(sf.statements[2])
            );
        } else {
            std::cout << "ERROR:: incorrect count statements for IF : " << sf.statements.size() << std::endl;
        }
    }
    if (!sf.statements.empty()) {
        return std::make_unique<LAst::FuncCallStatement>(
            "", std::move(sf.statements)
        );
    }
    std::cout << "Unhandled type" << std::endl;
    return nullptr;
}

void ReduceForm(std::vector<StackFrame>& stack) {
    StackFrame finished_sf = std::move(stack[stack.size() - 1]);
    stack.pop_back();
    StackFrame& prev_sf = stack[stack.size() - 1];
    if (finished_sf.type == StackFrameType::StackFrameType_ID_LIST) {
        for (const auto& name : finished_sf.names) {
            prev_sf.names.push_back(name);
        }
    } else {
        std::unique_ptr<LAst::LStatement> stmt = ConvertStackFrameToStatement(std::move(finished_sf));
        if (stmt) {
            prev_sf.statements.push_back(std::move(stmt));
        } else {
            std::cout << "ERROR::Can't create stmt" << std::endl;
        }
    }
}

std::vector<std::unique_ptr<LAst::LStatement>> BuildAst(PDA_Parser pdaParser, const std::vector<LLexer::LexToken>& ltokens) {
    std::vector<StackFrame> stack;
    StackFrame initStackFrame;
    stack.push_back(std::move(initStackFrame));
    size_t counter = 0;
    while (pdaParser.MoveNext())
    {
        if (pdaParser.LastParserAction() == PDA_ParserAction::PDA_ParserAction_Token) {
            LLexer::LexToken ltoken = ltokens[counter++];
            LSyntax::Token stoken = pdaParser.CurrentToken();

            // NOTE: Skip tokens
            if (stoken == SHARP_term) {
                continue;
            }

            StackFrame& sf = stack[stack.size() - 1];
            sf.counter++;
            if (stoken == OP_term) {
                StackFrame new_sf;
                stack.push_back(std::move(new_sf));
            } else if (stoken == CP_term) {
                ReduceForm(stack);
            } else if (stoken == DEFVAR_term) {
                sf.type = StackFrameType::StackFrameType_DEFVAR;
            } else if (stoken == DEFUN_term) {
                sf.type = StackFrameType::StackFrameType_DEFUN;
            } else if (stoken == QUOTE_term) {
                StackFrame new_sf;
                new_sf.type = StackFrameType::StackFrameType_QUOTE;
                stack.push_back(std::move(new_sf));
            } else if (stoken == PRINT_term) {
                sf.type = StackFrameType::StackFrameType_PRINT;
            } else if (stoken == LIST_term) {
                sf.type = StackFrameType::StackFrameType_LIST;
            } else if (stoken == RETURN_term) {
                sf.type = StackFrameType::StackFrameType_RETURN;
            } else if (stoken == IF_term) {
                sf.type = StackFrameType::StackFrameType_IF;
            } else if (stoken == ID_term) {
                if ((sf.counter == 0 && (sf.type == StackFrameType::StackFrameType_FUNCALL ||
                                         sf.type == StackFrameType::StackFrameType_QUOTE)) ||
                    (sf.counter <= 1 && (sf.type == StackFrameType::StackFrameType_DEFVAR ||
                                         sf.type == StackFrameType::StackFrameType_DEFUN))) {
                    sf.name = ltoken.lexem;

                    if (sf.type == StackFrameType::StackFrameType_QUOTE) {
                        ReduceForm(stack);
                    }
                } else if (sf.type == StackFrameType::StackFrameType_ID_LIST) {
                    sf.names.push_back(ltoken.lexem);
                } else {
                    sf.statements.push_back(
                        std::make_unique<LAst::VarValue>(ltoken.lexem)
                    );
                }
            } else if (stoken == NUMBER_term) {
                StackFrame& sf = stack[stack.size() - 1];
                sf.statements.push_back(
                    std::make_unique<LAst::NumericConst>(std::stod(ltoken.lexem))
                );
            } else if (stoken == STRING_term) {
                StackFrame& sf = stack[stack.size() - 1];
                sf.statements.push_back(
                    std::make_unique<LAst::StringConst>(ltoken.lexem)
                );
            } else if (stoken == T_term) {
                StackFrame& sf = stack[stack.size() - 1];
                sf.statements.push_back(
                    std::make_unique<LAst::BoolConst>(true)
                );
            } else if (stoken == NIL_term) {
                StackFrame& sf = stack[stack.size() - 1];
                sf.statements.push_back(
                    std::make_unique<LAst::BoolConst>(false)
                );
            }
            
            
        } else if (pdaParser.LastParserAction() == PDA_ParserAction_Rule) {
            if (pdaParser.CurrentRule().source == id_expr) {
                stack[stack.size() - 1].type = StackFrameType::StackFrameType_FUNCALL;
            } else if (pdaParser.CurrentRule().source == IDs) {
                stack[stack.size() - 1].type = StackFrameType::StackFrameType_ID_LIST;
            } else if (pdaParser.CurrentRule().source == lambda_expr) {
                stack[stack.size() - 1].type = StackFrameType::StackFrameType_LAMBDA;
            }
        }
    }
    bool success = pdaParser.IsValid();
    if (success) {
        // std::cout << "Parser result = Success" << std::endl;
    } else {
        std::cout << "Parser result = Failure" << std::endl;
    }
    if (stack.size() != 1) {
        std::cout << "ERROR:: stack size is " << stack.size() << std::endl;
    }

    return std::move(stack[0].statements);
}

void AddBuiltIns(LRuntime::Closure& closure) {
    // TODO: Implement
    // BR: List operations

    std::vector<std::unique_ptr<LAst::LStatement>> car_func_body;
    car_func_body.push_back(std::make_unique<LAst::Car>(
        std::make_unique<LAst::VarValue>("arg1")
    ));
    LRuntime::LFunction car_func("car", {"arg1"}, std::move(car_func_body));

    // TODO: Implement: cdr ,rest ,cons ,nth ,sort ,maplist ,mapcar
    // ...

    // BR: Arithm operations
    std::vector<std::unique_ptr<LAst::LStatement>> plus_func_body;
    plus_func_body.push_back(std::make_unique<LAst::Add>(
        std::make_unique<LAst::VarValue>("arg1"),
        std::make_unique<LAst::VarValue>("arg2")
    ));
    LRuntime::LFunction plus_func("+", {"arg1", "arg2"}, std::move(plus_func_body));

    // TODO: Implement: -, *, /, and,, or, not, <
    // ...

    closure["car"] = LRuntime::ObjectHolder::Own(std::move(car_func));
    // TODO: Uncomment
    // closure["cdr"] = LRuntime::ObjectHolder::Own(std::move(rest_func));
    // closure["rest"] = LRuntime::ObjectHolder::Own(std::move(rest_func));
    // closure["cons"] = LRuntime::ObjectHolder::Own(std::move(cons_func));
    // closure["nth"] = LRuntime::ObjectHolder::Own(std::move(nth_func));
    // closure["sort"] = LRuntime::ObjectHolder::Own(std::move(sort_func));
    // closure["maplist"] = LRuntime::ObjectHolder::Own(std::move(maplist_func));
    // closure["mapcar"] = LRuntime::ObjectHolder::Own(std::move(maplist_func));
    closure["+"] = LRuntime::ObjectHolder::Own(std::move(plus_func));
    // TODO: Uncomment
    // closure["-"] = LRuntime::ObjectHolder::Own(std::move(minus_func));
    // closure["*"] = LRuntime::ObjectHolder::Own(std::move(mult_func));
    // closure["/"] = LRuntime::ObjectHolder::Own(std::move(div_func));
    // closure["and"] = LRuntime::ObjectHolder::Own(std::move(and_func));
    // closure["or"] = LRuntime::ObjectHolder::Own(std::move(or_func));
    // closure["not"] = LRuntime::ObjectHolder::Own(std::move(not_func));
    // closure["<"] = LRuntime::ObjectHolder::Own(std::move(less_func));
}

void Execute(std::vector<std::unique_ptr<LAst::LStatement>> statements) {
    LRuntime::Closure closure;
    AddBuiltIns(closure);

    
    for (size_t i = 0; i < statements.size(); i++) {
        statements[i]->Execute(closure);
    }
}
