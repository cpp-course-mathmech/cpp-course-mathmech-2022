#pragma once

#include <iostream>
#include <vector>
#include <memory>
#include <unordered_map>
#include <string>
#include <list>


namespace LAst {
  class LStatement;
}

namespace LRuntime {

class ObjectHolder;


class Object {
public:
  virtual ~Object() = default;
  virtual void Print(std::ostream& os) const = 0;
};


template <typename T>
class ValueObject : public Object {
public:
  ValueObject(T v) : value(v) {
  }

  void Print(std::ostream& os) const override {
    os << value;
  }

  const T& GetValue() const {
    return value;
  }

private:
  T value;
};

using String = ValueObject<std::string>;
using Number = ValueObject<double>;
class Bool : public ValueObject<bool> {
public:
  using ValueObject<bool>::ValueObject;
  void Print(std::ostream& os) const override;
};

using Closure = std::unordered_map<std::string, ObjectHolder>;

class LFunction : public Object {
public:
  explicit LFunction(
    std::string name,
    std::vector<std::string> argument_names,
    std::vector<std::unique_ptr<LAst::LStatement>> body
  );
  void Print(std::ostream& os) const override;
  ObjectHolder Call(const std::vector<ObjectHolder>& actual_args, Closure& closure);
private:
  std::string name;
  std::vector<std::string> argument_names;
  std::vector<std::unique_ptr<LAst::LStatement>> body;
};

class LList : public Object {
public:
  std::list<ObjectHolder> args;
  explicit LList(
    std::list<ObjectHolder> args
  );
  void Print(std::ostream& os) const override;
  ObjectHolder Get(int i);
};


}