#include "grammar_transform.h"


std::set<LSyntax::Token> FindDirectLambdaRulesAndDeleteThem(std::set<LSyntax::Token>& sourcesToCheck, LSyntax::Grammar& grammar) {
    // Find direct Lambda rules and delete them
    std::set<LSyntax::Token> directLambdaNonTerminals;
    while(true) {
        std::set<LSyntax::Token> currentDirectLambdaNonTerminal = FindAndRemoveDirectLambdaNonTerminals(
                sourcesToCheck, grammar, directLambdaNonTerminals);
        if (currentDirectLambdaNonTerminal.size() == 0) {
            break;
        }
        for (const auto& source : currentDirectLambdaNonTerminal) {
            sourcesToCheck.erase(source);
        }
    }
    return directLambdaNonTerminals;
}

void FindLambdaNonTerminalsComp(std::set<LSyntax::Token>& sourcesToCheck, LSyntax::Grammar& grammar,
        std::set<LSyntax::Token>& lambdaNonTerminals) {
    // While not stable check and add to lambda rules
    while(true) {
        std::set<LSyntax::Token> currentLambdaNonTerminal = FindLambdaNonTerminals(
                sourcesToCheck, grammar, lambdaNonTerminals);
        if (currentLambdaNonTerminal.size() == 0) {
            break;
        }
        for (const auto& source : currentLambdaNonTerminal) {
            sourcesToCheck.erase(source);
        }
    }
}


void ReplaceNonTerminalsWithReductions(LSyntax::Grammar& grammar, const  std::set<LSyntax::Token>& lambdaNonTerminals) {
    // remove lambda rules and Make their reductions
    std::set<LSyntax::Rule> rulesToAdd;
    std::set<LSyntax::Rule> rulesToRemove;
    for (auto& rule_p : grammar.rules) {
        for (const auto& rule : rule_p.second) {
            std::set<LSyntax::Rule> reduction = MakeRuleReduction(rule, lambdaNonTerminals);
            rulesToRemove.insert(rule);
            rulesToAdd.insert(reduction.begin(), reduction.end());
        }
    }
    for (const auto& rule : rulesToRemove) {
        grammar.rules.at(rule.source).erase(rule);
    }
    
    for (const auto& rule : rulesToAdd) {
        grammar.rules.at(rule.source).insert(rule);
    }
}


LSyntax::Grammar MakeLambdaFree(const LSyntax::Grammar& grammar) {
    LSyntax::Grammar new_grammar;
    new_grammar.axiom = grammar.axiom;
    std::set<LSyntax::Token> sourcesToCheck;
    for(const auto& rule_p : grammar.rules) {
        const auto& source = rule_p.first;
        sourcesToCheck.insert(source);
        for(const auto& rule : rule_p.second) {
            std::vector<LSyntax::Token> rule_rhs(rule.value.begin(), rule.value.end());
            new_grammar.rules[source].insert(LSyntax::Rule(source, rule_rhs));
        }
    }

    // Find direct Lambda rules and delete them
    std::set<LSyntax::Token> lambdaNonTerminals = FindDirectLambdaRulesAndDeleteThem(sourcesToCheck, new_grammar);


    // While not stable check and add to lambda rules
    FindLambdaNonTerminalsComp(sourcesToCheck, new_grammar, lambdaNonTerminals);

    // remove lambda rules and Make their reductions
    ReplaceNonTerminalsWithReductions(new_grammar, lambdaNonTerminals);

    // remove sources with no rules
    std::set<LSyntax::Token> sourcesWithNoRules;
    for (const auto& rule_p : new_grammar.rules) {
        if (rule_p.second.size() == 0) {
            sourcesWithNoRules.insert(rule_p.first);
        }
    }
    for (const auto& source : sourcesWithNoRules) {
        new_grammar.rules.erase(source);
    }

    return new_grammar;
}

std::set<LSyntax::Token> FindAndRemoveDirectLambdaNonTerminals(
        std::set<LSyntax::Token> sourcesToCheck,
        LSyntax::Grammar& grammar,
        std::set<LSyntax::Token>& directLambdaNonTerminals) {
    std::set<LSyntax::Token> currentDirectLambdaSources;
    for(const auto& source : sourcesToCheck) {
        std::set<LSyntax::Rule> rulesToRemove;
        bool allRulesLambda = true;
        for(const auto& rule : grammar.rules.at(source)) {
            if (rule.value.size() == 1 && rule.value[0] == LSyntax::Lambda) {
                directLambdaNonTerminals.insert(source);
                currentDirectLambdaSources.insert(source);
                rulesToRemove.insert(rule);
            }
            if (!CheckAllLambdaTokens(rule, directLambdaNonTerminals)) {
                allRulesLambda = false;
            }
        }
        if (allRulesLambda) {
            directLambdaNonTerminals.insert(source);
            currentDirectLambdaSources.insert(source);
        }
        for (const auto& rule : rulesToRemove) {
            grammar.rules.at(source).erase(rule);
        }
    }

    return currentDirectLambdaSources;
}


std::set<LSyntax::Token> FindLambdaNonTerminals(
        std::set<LSyntax::Token> sourcesToCheck,
        const LSyntax::Grammar& grammar,
        std::set<LSyntax::Token>& lambdaNonTerminals) {
    std::set<LSyntax::Token> currentLambdaSources;
    for(const auto& source : sourcesToCheck) {
        for(const auto& rule : grammar.rules.at(source)) {
            if (CheckAllLambdaTokens(rule, lambdaNonTerminals)) {
                lambdaNonTerminals.insert(source);
                currentLambdaSources.insert(source);
            }
        }
    }
    return currentLambdaSources;
}

bool CheckAllLambdaTokens(const LSyntax::Rule& rule, const std::set<LSyntax::Token>& lambdaTokens) {
    for (const auto& token : rule.value) {
        if (token != LSyntax::Lambda || lambdaTokens.find(token) == lambdaTokens.end()) {
            return false;
        }
    }
    return true;
}

void MakeRuleReductionAt(size_t pos, std::vector<bool>& selected_positions, std::set<LSyntax::Rule>& reduction, const LSyntax::Rule& rule, const std::set<LSyntax::Token>& lambdaTokens) {
    if (pos == rule.value.size()) {
        LSyntax::Rule newRule;
        newRule.source = rule.source;
        for (size_t i = 0; i < rule.value.size(); ++i) {
            if (selected_positions[i]) {
                newRule.value.push_back(rule.value[i]);
            }
        }
        if (newRule.value.size() > 0) {
            reduction.insert(newRule);
        }
        return;
    }
    if (lambdaTokens.find(rule.value[pos]) != lambdaTokens.end()) {
        selected_positions[pos] = false;
        MakeRuleReductionAt(pos + 1, selected_positions, reduction, rule, lambdaTokens);
    }
    selected_positions[pos] = true;
    MakeRuleReductionAt(pos + 1, selected_positions, reduction, rule, lambdaTokens);
}

std::set<LSyntax::Rule> MakeRuleReduction(const LSyntax::Rule& rule, const std::set<LSyntax::Token>& lambdaTokens) {
    std::vector<bool> selected_positions(rule.value.size(), true);
    std::set<LSyntax::Rule> reductions;
    MakeRuleReductionAt(0, selected_positions, reductions, rule, lambdaTokens);
    return reductions;
}




void PrintTrieNode(std::ostream& out, const CTrieNode<LSyntax::Token, LSyntax::Rule>& node, int level) {
    out << std::string(level * 2, ' ');
    out << "[";
    for(const auto& c : node.part) {
        out << c;
    }
    out << "]\n";
    for(const auto& child : node.nodes) {
        out << std::string(level * 2, ' ') << child.first << "\n";
        PrintTrieNode(out, *child.second, level + 1);
    }
}

LSyntax::Grammar LeftFactorize(const LSyntax::Grammar& grammar) {
    std::map<std::string, int> term_name_to_max_sign = FindMaxSigns(grammar);

    LSyntax::Grammar new_grammar;
    new_grammar.axiom = grammar.axiom;

    for (const auto& p : grammar.rules) {
        CTrie<LSyntax::Token, LSyntax::Rule> trie;
        for (const auto& rule : p.second) {
            trie.Add(rule, rule.value);
        }
        int counter = 1;
        FactorizeNode(p.first, trie.root_node, new_grammar, counter, term_name_to_max_sign);
    }
    return new_grammar;
}

LSyntax::Token FactorizeNode(
    LSyntax::Token source,
    std::shared_ptr<CTrieNode<LSyntax::Token, LSyntax::Rule>> trie_node,
    LSyntax::Grammar& grammar, int& source_counter,
    std::map<std::string, int>& term_name_to_max_sign) {
    
    if (trie_node->nodes.size() == 0) {
        return LSyntax::Token();
    }

    std::set<std::vector<LSyntax::Token>> right_sides;
    for(const auto& token_p : trie_node->nodes) {
        LSyntax::Token token = token_p.first;
        std::vector<LSyntax::Token> right_side;
        LSyntax::Token new_sub_source = FactorizeNode(source, trie_node->nodes[token], grammar, source_counter, term_name_to_max_sign);
        right_side.push_back(token);
        if (new_sub_source.type == LSyntax::TokenType::NONE_TOKEN) {
            for(const auto& part_token : trie_node->nodes[token]->part) {
                right_side.push_back(part_token);
            }
        } else {
            right_side.push_back(new_sub_source);
        }
        right_sides.insert(right_side);
    }

    // create new source
    LSyntax::Token new_source = source;
    if (!trie_node->is_root) {
        int next_sign = ++term_name_to_max_sign[trie_node->value.source.value];
        new_source = LSyntax::Token(LSyntax::TokenType::NON_TERMINAL, trie_node->value.source.value, next_sign);
    }

    grammar.rules[new_source] = {};
    for(auto& rs : right_sides) {
        grammar.rules[new_source].insert(LSyntax::Rule(new_source, std::move(rs)));
    }

    if (trie_node->nodes.size() == 1 && !trie_node->is_root) {
        grammar.rules[new_source].insert(LSyntax::Rule(new_source, std::vector<LSyntax::Token>{LSyntax::Lambda}));
    }

    return new_source;
}


LSyntax::Grammar EliminateLeftRecursion(const LSyntax::Grammar& grammar) {
    std::map<std::string, int> term_name_to_max_sign = FindMaxSigns(grammar);
    LSyntax::Grammar new_grammar;
    new_grammar.axiom = grammar.axiom;
    
    for(const auto& rule_p : grammar.rules) {
        const auto& source = rule_p.first;
        for(const auto& rule : rule_p.second) {
            std::vector<LSyntax::Token> rule_rhs(rule.value.begin(), rule.value.end());
            new_grammar.rules[source].insert(LSyntax::Rule(source, rule_rhs));
        }
    }

    std::vector<LSyntax::Token> sources;
    for (const auto& p : new_grammar.rules) {
        sources.push_back(p.first);
    }

    for (const auto& source : sources) {
        using LSyntax::Token;
        using LSyntax::TokenType;
        using LSyntax::Rule;

        bool stable = false;
        int counter = 0;
        while(!stable) {
            counter++;

            stable = true;
            std::set<Rule> to_expand;
            std::set<Rule> to_remove;
            for(const auto& rule : new_grammar.rules.at(source)) {
                Token first_token = rule.value[0];
                if (first_token.type == TokenType::NON_TERMINAL &&
                    first_token < source) {
                    
                    if (new_grammar.rules.find(first_token) == new_grammar.rules.end()) {
                        continue;
                    }

                    stable = false;
                    Unwrap(rule, first_token, new_grammar, to_expand);
                    to_remove.insert(rule);
                }
            }

            for(const auto& rule_to_remove : to_remove) {
                new_grammar.rules[source].erase(rule_to_remove);
            }

            for(const auto& rule_to_add : to_expand) {
                new_grammar.rules[source].insert(rule_to_add); 
            }
        }
        EliminateDirectLeftRecursion(source, new_grammar, term_name_to_max_sign);
    }
    return new_grammar;
}

void Unwrap(
    LSyntax::Rule rule_to_expand,
    LSyntax::Token to_unwrap,
    const LSyntax::Grammar& grammar,
    std::set<LSyntax::Rule>& new_rules) {
    
    for (const auto& rule : grammar.rules.at(to_unwrap)) {
        std::vector<LSyntax::Token> right_side(rule.value.begin(), rule.value.end());
        for (size_t i = 1; i < rule_to_expand.value.size(); ++i) {
            right_side.push_back(rule_to_expand.value[i]);
        }
        new_rules.insert(LSyntax::Rule(rule_to_expand.source, right_side));
    }
}

std::map<std::string, int> FindMaxSigns(const LSyntax::Grammar& grammar) {
    std::map<std::string, int> term_name_to_max_sign;
    for(const auto& rule_p : grammar.rules) {
        const auto& source = rule_p.first;
        if (source.sign > term_name_to_max_sign[source.value]) {
            term_name_to_max_sign[source.value] = source.sign;
        }
        for (const auto& rule : rule_p.second) {
            for (const auto& token : rule.value) {
                if (token.sign > term_name_to_max_sign[token.value]) {
                    term_name_to_max_sign[token.value] = token.sign;
                }
            }
        }
    }

    return term_name_to_max_sign;
}

void EliminateDirectLeftRecursion(LSyntax::Token source, LSyntax::Grammar& grammar, std::map<std::string, int>& term_name_to_max_sign) {
    using LSyntax::Token;
    using LSyntax::TokenType;
    using LSyntax::Rule;

    if (!CheckLeftRecursion(source, grammar)) {
        return;
    }

    
    int next_sign = ++term_name_to_max_sign[source.value];

    LSyntax::Token new_source(LSyntax::TokenType::NON_TERMINAL, source.value, next_sign);
    if (grammar.rules.find(new_source) == grammar.rules.end()) {
        grammar.rules[new_source] = {};
    }
    std::vector<Rule> rules(grammar.rules[source].begin(), grammar.rules[source].end());
    for(auto& rule : rules) {
        if (rule.value[0] == source) {
            grammar.rules[source].erase(rule);
            std::vector<Token> rest;
            for (size_t i = 1; i < rule.value.size(); ++i) {
                rest.push_back(rule.value[i]);
            }
            rest.push_back(new_source);
            grammar.rules[new_source].insert(Rule(new_source, rest));
        } else {
            grammar.rules[source].erase(rule);
            rule.value.push_back(new_source);
            grammar.rules[source].insert(rule);
        }
    }
     grammar.rules[new_source].insert(Rule(new_source, {LSyntax::Lambda}));
}

bool CheckLeftRecursion(LSyntax::Token source, LSyntax::Grammar& grammar) {
    for(const auto& rule : grammar.rules[source]) {
        if (rule.value[0] == source) {
            return true;
        }
    }
    return false;
}

using FIRST = typename std::map<LSyntax::Token, std::set<LSyntax::Token>>;
using FOLLOW = typename std::map<LSyntax::Token, std::set<LSyntax::Token>>;
using SELECT = typename std::map<LSyntax::Rule, std::set<LSyntax::Token>>;

bool CalcFirstForRule(const LSyntax::Rule& rule, FIRST& first);

void PrintFirst(std::ostream& out, FIRST& first) {
    for(const auto& p : first) {
        out << p.first << " = {";
        for(const auto& term : p.second) {
            out << term << ", ";
        }
        out << "}\n";
    }
}

void PrintSelect(std::ostream& out, SELECT& select) {
    for(const auto& p : select) {
        out << p.first << " = {";
        for(const auto& term : p.second) {
            out << term << ", ";
        }
        out << "}\n";
    }
}


// FIRST
std::map<LSyntax::Token, std::set<LSyntax::Token>> CalcFirst(const LSyntax::Grammar& grammar) {
    FIRST first;
    bool stable = false;
    int counter = 0;
    SetFirstOfTerminals(grammar, first);

    while(!stable) {
        stable = true;
        counter++;

        for(const auto& rule_p : grammar.rules) {
            for (const auto& rule : rule_p.second) {
                bool modified = CalcFirstForRule(rule, first);
                if (modified) {
                    stable = false;
                }
            }
        }
    }

    return first;
}

void SetFirstOfTerminals(const LSyntax::Grammar& grammar, FIRST& first) {
    for(const auto& rule_p : grammar.rules) {
        for (const auto& rule : rule_p.second) {
            for (const auto& token : rule.value) {
                if (token.type == LSyntax::TERMINAL) {
                    first[token].insert(token);
                }
            }
        }
    }
}

bool CalcFirstForRule(const LSyntax::Rule& rule, FIRST& first) {
    if (rule.source == rule.value[0]) return false;
    bool modified = false;
    bool lambda_in_first = false;
    for(size_t i = 0; i < rule.value.size(); i++) {
        const auto& token = rule.value[i];
        lambda_in_first = false;
        if (token.type == LSyntax::TokenType::TERMINAL) {
            const auto& res = first[rule.source].insert(token);
            if (res.second) {
                modified = true;
                break;
            }
        } else {
            for (const auto& term : first[token]) {
                if (term == LSyntax::Lambda) {
                    lambda_in_first = true;
                    continue;
                }
                const auto& res = first[rule.source].insert(term);
                if (res.second) {
                    modified = true;
                }
            }
        }

        if (!lambda_in_first) break;
    }
    if (lambda_in_first) {
        first[rule.source].insert(LSyntax::Lambda);
    }
    return modified;
}


// FOLLOW
FOLLOW CalcFollow(const LSyntax::Grammar& grammar, const FIRST& first) {
    FOLLOW follow;
    bool stable = false;
    int counter = 0;

    follow[grammar.axiom].insert(LSyntax::EndOfStr);
    while(!stable) {
        stable = true;
        counter++;

        for(const auto& rule_p : grammar.rules) {
            for (const auto& rule : rule_p.second) {
                bool modified = CalcFollowForRule(rule, follow, first);
                if (modified) {
                    stable = false;
                }
            }
        }
    }

    return follow;
}


bool CalcFollowForRule(const LSyntax::Rule& rule, FOLLOW& follow, const FIRST& first) {
    bool modified = false;
    bool lambda_at_rest = true;
    int rule_len = rule.value.size();
    for(int i = rule_len - 1; i >= 0; i--) {
        const auto& token = rule.value[i];

        if (token.type == LSyntax::TERMINAL && token != LSyntax::Lambda) {
            lambda_at_rest = false;
            continue;
        }

        if (lambda_at_rest) {
            for (const auto& term : follow[rule.source]) {
                const auto& res = follow[token].insert(term);
                if (res.second) modified = true;
            }
        }

        if (i == rule_len - 1) {
            if (first.find(token) == first.end()) {
                lambda_at_rest = false;
            }
            continue;
        }

        const auto& next_token = rule.value[i + 1];

        bool saw_lambda = false;
        for(const auto& term : first.at(next_token)) {
            if (term != LSyntax::Lambda) {
                const auto& res = follow[token].insert(term);
                if (res.second) modified = true;
            } else {
                saw_lambda = true;
            }
        }

        if (saw_lambda) {
            std::set<LSyntax::Token> tokensToAdd;
            for(const auto& term : follow[next_token]) {
                tokensToAdd.insert(term);
            }
            for(const auto& term : tokensToAdd) {
                if (term != LSyntax::Lambda) {
                    const auto& res = follow[token].insert(term);
                    if (res.second) modified = true;
                }
            }
        }

        if (!saw_lambda) {
            lambda_at_rest = false;
        }
    }

    return modified;
}


// SELECT
SELECT CalcSelect(const LSyntax::Grammar& grammar, const FIRST& first, const FOLLOW& follow) {
    SELECT select;

    for(const auto& rule_p : grammar.rules) {
        for (const auto& rule : rule_p.second) {
            CalcSelect(rule, first, follow, select);
        }
    }

    return select;
}

void CalcSelect(const LSyntax::Rule& rule, const FIRST& first, const FOLLOW& follow, SELECT& select) {
    select[rule];
    bool lambda_in_first = true;
    for(size_t i = 0; i < rule.value.size(); i++) {
        const auto& token = rule.value[i];

        bool saw_lambda = first.at(token).find(LSyntax::Lambda) != first.at(token).end();
        if (!saw_lambda || !lambda_in_first) {
            lambda_in_first = false;
        }

        for (const auto& term : first.at(token)) {
            if (term != LSyntax::Lambda) {
                select[rule].insert(term);
            }
        }

        if (!saw_lambda) {
            break;
        }
    }

    if (lambda_in_first) {
        for (const auto& term : follow.at(rule.source)) {
            select[rule].insert(term);
        }
    }
}

// LL(1) Check
bool IsLL1(const LSyntax::Grammar& grammar, const SELECT& select) {
    for(const auto& rule_p : grammar.rules) {
        const auto& source = rule_p.first;

        std::set<LSyntax::Token> total_for_source;
        for (const auto& rule : rule_p.second) {
            if (select.find(rule) == select.end()) {
                std::cout << "Rule not found in SELECT: " << rule << std::endl;
            }
            for(const auto& token : select.at(rule)) {
                const auto& res = total_for_source.insert(token);
                if (!res.second) {
                    std::cout << "For " << source << " Token: " << token << std::endl;
                    return false;
                }
            }
        }
    }
    return true;
}


// PARSING TABLE
using PARSING_TABLE = typename std::map<LSyntax::Token, std::map<LSyntax::Token, LSyntax::Rule>>;

void PrintParsingTable(std::ostream& out, PARSING_TABLE& parse_table) {
    for(const auto& p : parse_table) {
        const auto& source = p.first;
        for(const auto& term_to_rule : p.second) {
            const auto& term = term_to_rule.first;
            const auto& rule = term_to_rule.second;
            out << "(" << source << ", " << term << ")  ==>  " << rule << std::endl;
        }
        out << "\n";
    }
}



PARSING_TABLE CalcParsingTable(const LSyntax::Grammar& grammar, const SELECT& select) {
    PARSING_TABLE parsing_table;
    for(const auto& rule_p : grammar.rules) {
        for (const auto& rule : rule_p.second) {
            for (const auto& first_term : select.at(rule)) {
                parsing_table[rule.source][first_term] = rule;
            }
        }
    }
    return parsing_table;
}
