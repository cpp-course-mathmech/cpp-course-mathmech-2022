#include <catch2/catch_all.hpp>


#include "add.h"


TEST_CASE("SimpleTestPublic1", "") {
    REQUIRE(add(1, 9) == 10);
}

TEST_CASE("SimpleTestPublic2", "") {
    REQUIRE(add(3, -3) == 0);
}

