#pragma once


#include <string>
#include <map>
#include <unordered_set>
#include <vector>
#include <deque>
#include <tuple>
#include <istream>
#include <ostream>
#include <iostream>


namespace LLexer {

enum class LexTokenType {
    LEX_UNK,
    LEX_OP,
    LEX_CP,
    LEX_ID,
    LEX_NUMBER,
    LEX_CHARACTER,
    LEX_QUOTE,
    LEX_BACKQUOTE,
    LEX_COMMA,
    LEX_LITERAL,
    LEX_KEYWORD,
    LEX_T,
    LEX_NIL,
    LEX_END,

    LEX_AND,
    LEX_OR,
    LEX_IF,
    LEX_WHEN,
    LEX_UNLESS,
    LEX_COND,
    LEX_LOOP,
    LEX_FOR,
    LEX_FROM,
    LEX_TO,
    LEX_DO,
    LEX_DOTIMES,
    LEX_RETURN,
    LEX_VALUES,
    LEX_SET,
    LEX_SETQ,
    LEX_SETF,
    LEX_LET,
    LEX_LAMBDA,
    LEX_DEFUN,
    LEX_DEFVAR,
    LEX_DEFMACRO,
    LEX_DEFCLASS,
    LEX_DEFGENERIC,
    LEX_DEFMETHOD,
    LEX_DEFSTRUCT,
    LEX_DEFPARAMETER,
    LEX_FLET,
    LEX_MAKE_ARRAY,
    LEX_MAKE_HASH_TABLE,
    LEX_PRINT,
    LEX_LIST,
    LEX_SHARP,
};




class LexToken {
public:
    LexTokenType type;
    std::string lexem;
    LexToken(LexTokenType type, std::string lexem);
};


bool operator==(const LexToken& lhs, const LexToken& rhs);


class Tokenizer {
public:
    std::deque<LexToken> token_queue;
    std::map<std::string, LexTokenType> keywords = {
        {"t", LexTokenType::LEX_T},
        {"nil", LexTokenType::LEX_NIL},
        {"and", LexTokenType::LEX_AND},
        {"or", LexTokenType::LEX_OR},
        {"if", LexTokenType::LEX_IF},
        {"when", LexTokenType::LEX_WHEN},
        {"unless", LexTokenType::LEX_UNLESS},
        {"cond", LexTokenType::LEX_COND},
        {"loop", LexTokenType::LEX_LOOP},
        {"for", LexTokenType::LEX_FOR},
        {"from", LexTokenType::LEX_FROM},
        {"to", LexTokenType::LEX_TO},
        {"do", LexTokenType::LEX_DO},
        {"dotimes", LexTokenType::LEX_DOTIMES},
        {"return", LexTokenType::LEX_RETURN},
        {"values", LexTokenType::LEX_VALUES},
        {"set" , LexTokenType::LEX_SET},
        {"setq" , LexTokenType::LEX_SETQ},
        {"setf" , LexTokenType::LEX_SETF},
        {"let" , LexTokenType::LEX_LET},
        {"lambda" , LexTokenType::LEX_LAMBDA},
        {"defun" , LexTokenType::LEX_DEFUN},
        {"defvar" , LexTokenType::LEX_DEFVAR},
        {"defmacro" , LexTokenType::LEX_DEFMACRO},
        {"defclass" , LexTokenType::LEX_DEFCLASS},
        {"defgeneric" , LexTokenType::LEX_DEFGENERIC},
        {"defmethod" , LexTokenType::LEX_DEFMETHOD},
        {"defstruct" , LexTokenType::LEX_DEFSTRUCT},
        {"defparameter" , LexTokenType::LEX_DEFPARAMETER},
        {"flet" , LexTokenType::LEX_FLET},
        {"make-array" , LexTokenType::LEX_MAKE_ARRAY},
        {"make-hash-table" , LexTokenType::LEX_MAKE_HASH_TABLE},
        {"print" , LexTokenType::LEX_PRINT},
        {"list" , LexTokenType::LEX_LIST},
    };
    LexToken ReadNext(std::istream& in);
private:
    std::string ReadId(std::istream& in);
    std::string ReadNumber(std::istream& in);
    std::string ReadLiteral(std::istream& in);
    void SkipSpaces(std::istream& in);
};

}