#include <catch2/catch_all.hpp>

#include "spendings.h"

#include <algorithm>
#include <iostream>
#include <sstream>
#include <vector>

using namespace Xml;

TEST_CASE("libXml::Public::TestLoadFromXml", "") {
  std::istringstream xml_input(R"(<johan>
    <spend amount="2500" category="apples"></spend>
    <spend amount="1150" category="books"></spend>
    <spend amount="5780" category="coffee"></spend>
    <spend amount="7500" category="tea"></spend>
    <spend amount="12345" category="tickets"></spend>
    <spend amount="1100" category="flowers"></spend>
  </johan>)");

  const std::vector<Spending> spendings = LoadFromXml(xml_input);

  const std::vector<Spending> expected = {
    {"apples", 2500},
    {"books", 1150},
    {"coffee", 5780},
    {"tea", 7500},
    {"tickets", 12345},
    {"flowers", 1100}
  };
  REQUIRE(spendings == expected);
}
