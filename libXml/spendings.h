#pragma once

#include "xml.h"

#include <algorithm>
#include <iostream>
#include <sstream>
#include <vector>

struct Spending {
    std::string category;
    int amount;
};

bool operator == (const Spending& lhs, const Spending& rhs);

std::ostream& operator << (std::ostream& os, const Spending& s);

std::vector<Spending> LoadFromXml(std::istream& input);
